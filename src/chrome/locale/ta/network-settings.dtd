<!ENTITY torsettings.dialog.title "Tor நெட்வொர்க் அமைப்புகள்">
<!ENTITY torsettings.wizard.title.default "Tor உடன் இணை">
<!ENTITY torsettings.wizard.title.configure "Tor நெட்வொர்க் அமைப்புகள்">
<!ENTITY torsettings.wizard.title.connecting "இணைப்பை நிறுவுகிறது">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor உலாவி மொழி">
<!ENTITY torlauncher.localePicker.prompt "ஒரு மொழியைத் தேர்ந்தெடுங்கள்.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Tor உடன் இணைக்க &quot;இணையை&quot; கிளிக் செய்யுங்கள்.">
<!ENTITY torSettings.configurePrompt "நீங்கள் Tor ஐ தணிக்கை செய்யும் (எகிப்து, சீனா, துருக்கி போன்ற) நாடுகளில் இருந்தால் அல்லது பதிலாள் தேவைப்படும் தனிப்பட்ட வலைப்பின்னலில் இருந்தால் பிணைய அமைப்புகளைச் சரிசெய்ய &quot;கட்டமையை&quot; கிளிக் செய்யுங்கள்.">
<!ENTITY torSettings.configure "கட்டமைக்க">
<!ENTITY torSettings.connect "இணைக்க">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Tor தொடங்குவதற்கு காத்திருக்கிறது...">
<!ENTITY torsettings.restartTor "Tor ஐ மறுதொடக்கு">
<!ENTITY torsettings.reconfigTor "மறுகட்டமை">

<!ENTITY torsettings.discardSettings.prompt "நீங்கள் Tor bridges கட்டமைத்துள்ளீர்கள் அல்லது உள் பதிலாள் அமைப்பில் நுழைந்துள்ளீர்கள்.&#160; Tor பிணையத்துடன் நேரடி இணைப்பு ஏற்படுத்த, இந்த அமைப்புகள் நீக்கப்பட வேண்டும்.">
<!ENTITY torsettings.discardSettings.proceed "அமைப்புகளை நீக்கி இணை">

<!ENTITY torsettings.optional "விருப்பத்தேர்வு">

<!ENTITY torsettings.useProxy.checkbox "நான் இணையத்துடன் இணைய ஒரு பதிலாளைப் பயன்படுத்துகிறேன்">
<!ENTITY torsettings.useProxy.type "பதிலாள் வகை">
<!ENTITY torsettings.useProxy.type.placeholder "ஒரு பதிலாள் வகையைத் தேர்ந்தெடுங்கள்">
<!ENTITY torsettings.useProxy.address "முகவரி">
<!ENTITY torsettings.useProxy.address.placeholder "IP முகவரி அல்லது ஹோஸ்ட்பெயர்">
<!ENTITY torsettings.useProxy.port "பெயர்த்துவை">
<!ENTITY torsettings.useProxy.username "பயனர்பெயர்">
<!ENTITY torsettings.useProxy.password "கடவுச்சொல்">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "இந்த கணினி ஒரு தீயரண் வழியே செல்கிறது அது குறிப்பிட்ட முனைகளுக்கான இணைப்புகளை மட்டுமே அனுமதிக்கிறது">
<!ENTITY torsettings.firewall.allowedPorts "அனுமதிக்கப்பட்ட முனைகள்">
<!ENTITY torsettings.useBridges.checkbox "Tor எனது நாட்டின் தணிக்கையிடப்படுகிறது">
<!ENTITY torsettings.useBridges.default "ஒரு உள்ளமை bridge தேர்ந்தெடுங்கள்">
<!ENTITY torsettings.useBridges.default.placeholder "ஒரு bridge தேர்ந்தெடுங்கள்">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org இலிருந்து bridge கோருங்கள்">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "படத்திலிருக்கும் எழுத்துகளை உள்ளிடுங்கள்">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "ஒரு புதிய சவாலைப் பெறுங்கள்">
<!ENTITY torsettings.useBridges.captchaSubmit "சமர்ப்பி">
<!ENTITY torsettings.useBridges.custom "எனக்குத் தெரிந்த bridge வழங்கு">
<!ENTITY torsettings.useBridges.label "நம்பிக்கையான மூலத்திலிருந்து bridge தகவலை உள்ளிடுங்கள்.">
<!ENTITY torsettings.useBridges.placeholder "முகவரி:முனை தட்டச்சிடுங்கள் (ஒரு வரிசைக்கு ஒன்று)">

<!ENTITY torsettings.copyLog " Tor பதிவுகளை கிளிப்போர்டுக்கு நகலெடு">

<!ENTITY torsettings.proxyHelpTitle "பதிலாள் உதவி">
<!ENTITY torsettings.proxyHelp1 "ஒரு நிறுவனம், பள்ளி அல்லது பல்கலைக்கழக வலைப்பின்னல் வழியே இணைக்கும்போது ஒரு உள் பதிலாள் தேவைப்படலாம்.&#160;பதிலாள் தேவைப்படுகிறதா என்பது உங்களுக்கு உறுதியாகத் தெரியவில்லையெனில், மற்றொரு உலாவியின் இணைய அமைப்புகளைப் பாருங்கள் அல்லது உங்கள் கணினியின் பிணைய அமைப்புகளைச் சரிபாருங்கள்.">

<!ENTITY torsettings.bridgeHelpTitle "Bridge தொடர் உதவி">
<!ENTITY torsettings.bridgeHelp1 "Bridges என்பது பட்டியலிடப்படாத தொடர்கள் ஆகும் அவை Tor பிணையத்திற்கான இணைப்புகளைத் தடுப்பது இன்னும் கடினமாக்குகின்றன.&#160; ஒவ்வொரு வகை bridge உம் தணிக்கையைத் தவிர்க்க வெவ்வேறு முறையைப் பயன்படுத்துகிறது.&#160; obfs உங்கள் போக்குவரத்தை ஒரு சமவாய்ப்பு இரைச்சலாகத் தோன்றச் செய்கிறது, மேலும் Tor க்கு பதிலாக அதன் சேவயைஉடன் இணைப்பது போல் உங்கள் போக்குவரத்தைத் தோன்றச் செய்கிறது.">
<!ENTITY torsettings.bridgeHelp2 "குறிப்பிட்ட நாடுகள் Tor ஐ எப்படி முடக்க முயற்சிக்கின்றன என்பதைப் பொறுத்து , குறிப்பிட்ட bridges குறிப்பிட்ட நாடுகளில் மட்டுமே வேலை செய்கின்றன.&#160; உங்கள் நாட்டில் எந்த bridges வேலை செய்யும் என்பது உங்களுக்கு உறுதியாகத் தெரியவில்லையெனில், torproject.org/about/contact.html#support பக்கத்தைப் பாருங்கள்">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "தயவுசெய்து நாங்கள் Tor வலையமைப்புடன் ஒரு இணைப்பு நிறுவும்வரை காத்திருங்கள்.&#160;
இதற்கு பல நிமிடங்கள் ஆகலாம்.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "இணைப்பு">
<!ENTITY torPreferences.torSettings "Tor அமைப்புகள்">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser routes your traffic over the Tor Network, run by thousands of volunteers around the world." >
<!ENTITY torPreferences.learnMore "மேலும் அறிய">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "இணைப்பில்">
<!ENTITY torPreferences.statusInternetOffline "Offline">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "இணைக்கப்பட்டது">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "மேலும் அறிய">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Bridgeகள்">
<!ENTITY torPreferences.bridgesDescription "Bridges help you access the Tor Network in places where Tor is blocked. Depending on where you are, one bridge may work better than another.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatic">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "நீக்கு">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Copied!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Request a Bridge…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "மேம்பட்டது">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "பதிகைகளைக் காண்க...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "நிராகரி">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake ">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Bridge ஐ வேண்டுக">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contacting BridgeDB. Please Wait.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Solve the CAPTCHA to request a bridge.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "உள்ளிட்ட தீர்வு சரியானதல்ல. தயவு செய்து மீண்டும் முயல்க">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor பதிகைகள்">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "இணைக்கிறது...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatic">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "மீண்டும் முயற்சிக்கவும்">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
