<!ENTITY torsettings.dialog.title "Tor ցանցի կարգավորումներ">
<!ENTITY torsettings.wizard.title.default "Միանալ Tor-ին">
<!ENTITY torsettings.wizard.title.configure "Tor ցանցի կարգավորումներ">
<!ENTITY torsettings.wizard.title.connecting "Հիմնվում է միացում">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor դիտարկիչի լեզուն">
<!ENTITY torlauncher.localePicker.prompt "Խնդրում ենք ընտրել լեզուն։">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Սեղմեք «Միացնել» Tor-ին միանալու համար։">
<!ENTITY torSettings.configurePrompt "Սեղմեք «Կարգավորել» ցանցի կարգավորումները հարմարեցնելու համար՝ եթե դուք գտնվում եք Tor-ը գրաքննող երկրում (ինչպիսիք են՝ Եգիպտոսը, Չինաստանը, Թուրքիան) կամ միանում եք փոխանորդ պահանջող մասնավոր ցանցից:">
<!ENTITY torSettings.configure "Կարգավորել">
<!ENTITY torSettings.connect "Միացնել">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Սպասվում է Tor-ի սկիզբը…">
<!ENTITY torsettings.restartTor "Վերսկսել Tor-ը">
<!ENTITY torsettings.reconfigTor "Վերակարգավորել">

<!ENTITY torsettings.discardSettings.prompt "Դուք կարգավորել եք Tor կամուրջները կամ տեղական փոխանորդը:&#160; Tor ցանցին անմիջական միանալու համար այդ կարգավորումները պետք է հեռացվեն:">
<!ENTITY torsettings.discardSettings.proceed "Հեռացնել կարգավորումները և միանալ">

<!ENTITY torsettings.optional "Կամընտրական">

<!ENTITY torsettings.useProxy.checkbox "Համացանցին միանալու համար օգտվում եմ փոխանորդից">
<!ENTITY torsettings.useProxy.type "Փոխանորդի տեսակ">
<!ENTITY torsettings.useProxy.type.placeholder "ընտրեք փոխանորդի տեսակը">
<!ENTITY torsettings.useProxy.address "Հասցե">
<!ENTITY torsettings.useProxy.address.placeholder "IP հասցե կամ հյուրընկալի անուն">
<!ENTITY torsettings.useProxy.port "Մատույց">
<!ENTITY torsettings.useProxy.username "Օգտագործողի անուն">
<!ENTITY torsettings.useProxy.password "Գաղտնաբառ">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Այս համակարգիչը անցնում է միայն որոշ մատույցներ թույլատրող հրապատի միջով">
<!ENTITY torsettings.firewall.allowedPorts "Թույլտատրված մատույցներ">
<!ENTITY torsettings.useBridges.checkbox "Tor-ը գրաքննվում է իմ երկրում">
<!ENTITY torsettings.useBridges.default "Ընտրել ներկառուցված կամուրջ">
<!ENTITY torsettings.useBridges.default.placeholder "ընտրեք կամուրջը">
<!ENTITY torsettings.useBridges.bridgeDB "Պահանջել կամուրջ torproject.org-ից">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Մուտքագրեք նկարում պատկերված խորհրդանիշները">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Ստանալ նոր մարտահրավեր">
<!ENTITY torsettings.useBridges.captchaSubmit "Ուղարկել">
<!ENTITY torsettings.useBridges.custom "Տրամադրել իմ իմացած կամուրջները">
<!ENTITY torsettings.useBridges.label "Մուտքագրել թույլատված աղբյուրից կամուջի տեղեկությունը">
<!ENTITY torsettings.useBridges.placeholder "type address:port (one per line)">

<!ENTITY torsettings.copyLog "Պատճենել Tor-ի մատյանը սեղանակատախտակի վրա">

<!ENTITY torsettings.proxyHelpTitle "Փոխանորդի վերաբերյալ օգնություն">
<!ENTITY torsettings.proxyHelp1 "A local proxy might be needed when connecting through a company, school, or university network.&#160;If you are not sure whether a proxy is needed, look at the Internet settings in another browser or check your system's network settings.">

<!ENTITY torsettings.bridgeHelpTitle "Կամուրջի հերթափոխի վերաբերյալ օգնություն">
<!ENTITY torsettings.bridgeHelp1 "Bridges are unlisted relays that make it more difficult to block connections to the Tor Network.&#160; Each type of bridge uses a different method to avoid censorship.&#160; The obfs ones make your traffic look like random noise, and the meek ones make your traffic look like it's connecting to that service instead of Tor.">
<!ENTITY torsettings.bridgeHelp2 "Because of how certain countries try to block Tor, certain bridges work in certain countries but not others.&#160; If you are unsure about which bridges work in your country, visit torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Խնդրում ենք սպասել մինչև Tor ցանցի հետ միացման հիմնումը:&#160; Այն կարող է տևել մի քանի րոպե:">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Կապ">
<!ENTITY torPreferences.torSettings "Tor կարգավորումներ">
<!ENTITY torPreferences.torSettingsDescription "Tor դիտարկիչը ուղղորդում է Ձեր երթևեկը Tor ցանցի միջոցով, որը գործարկում են հազարավոր կամավորներ ամբողջ աշխարհում: " >
<!ENTITY torPreferences.learnMore "Իմանալ ավելին">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Համացանցը՝">
<!ENTITY torPreferences.statusInternetTest "Փորձարկել">
<!ENTITY torPreferences.statusInternetOnline "Առցանց">
<!ENTITY torPreferences.statusInternetOffline "Անցանց">
<!ENTITY torPreferences.statusTorLabel "Tor ցանցը՝">
<!ENTITY torPreferences.statusTorConnected "Միացվեց">
<!ENTITY torPreferences.statusTorNotConnected "Չի միացվել">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Իմանալ ավելին">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "«Quickstart»">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart-ը միացնում է Tor դիտարիչը Tor ցանցին ինքնաբերաբար, երբ մեկնարկում է, օգտագործողի վերջին կարգավորումների հիման վրա։">
<!ENTITY torPreferences.quickstartCheckbox "Միշտ միանալ ինքնաբերաբար">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Կամուրջներ">
<!ENTITY torPreferences.bridgesDescription "Կամուրջներն օգնում են Ձեզ Tor-ը արգելափակված տեղերում Tor ցանցի մատչելիություն ունենալ: Կախված նրանից թե որտեղ եք, մի կամուրջ կարող է ավելի լավ աշխատել քան մյուսը:">
<!ENTITY torPreferences.bridgeLocation "Ձեր գտնվելու վայրը">
<!ENTITY torPreferences.bridgeLocationAutomatic "Ինքնաբերաբար">
<!ENTITY torPreferences.bridgeLocationFrequent "Հաճախակի ընտրված գտնվելու վայրեր">
<!ENTITY torPreferences.bridgeLocationOther "Այլ գտնվելու վայրեր">
<!ENTITY torPreferences.bridgeChooseForMe "Ընտրել կամուրջ ինձ համար...">
<!ENTITY torPreferences.bridgeBadgeCurrent "Ձեր ներկայիս կամուրջները">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Կարող եք պահպանել մեկ կամ ավելի կամուրջ, և Tor-ը կընտրի թե որ մեկն օգտագործել Ձեր միանալու ժամանակ: Tor-ը անհրաժեշտության դեպքում ինքնաբերաբար կհերթագայի մեկ այլ կամուրջ օգտագործելու:">
<!ENTITY torPreferences.bridgeId "#1 կամուրջ՝ #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Հեռացնել">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Կարողազրկել ներկառուցված կամուրջները">
<!ENTITY torPreferences.bridgeShare "Տարածեք այս կամուրջն QR ծածկագրի օգնությամբ կամ պատճենելով դրա հասցեն.">
<!ENTITY torPreferences.bridgeCopy "Պատճենել կամուրջի հասցեն">
<!ENTITY torPreferences.copied "Պատճենվե՜ց:">
<!ENTITY torPreferences.bridgeShowAll "Ցուցադրել բոլոր կամուրջները">
<!ENTITY torPreferences.bridgeRemoveAll "Հեռացնել բոլոր կամուրջները">
<!ENTITY torPreferences.bridgeAdd "Ավելացնել նոր կամուրջ">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Ընտրել Tor Browser-ի ներկառուցված կամուրջներից մեկը">
<!ENTITY torPreferences.bridgeSelectBuiltin "Ընտրել ներկառուցված կամուրջ...">
<!ENTITY torPreferences.bridgeRequest "Պահանջել կամուրջ...">
<!ENTITY torPreferences.bridgeEnterKnown "Մուտքագրել Ձեր իմացած կամուրջը">
<!ENTITY torPreferences.bridgeAddManually "Ավելացնել կամուրջ ձեռքով...">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Ընդլայնված">
<!ENTITY torPreferences.advancedDescription "Կարգավորել թե ինչպես Tor դիտարկիչը միանա համացանցին">
<!ENTITY torPreferences.advancedButton "Կարգավորումներ...">
<!ENTITY torPreferences.viewTorLogs "Տեսնել Tor-ի մատյանները">
<!ENTITY torPreferences.viewLogs "Տեսնել մատյանները...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Հեռացնե՞լ բոլոր կամուրջները:">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Չեղարկել">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Ծրիր QR ծածկագիրը">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Ներկառուցված կամուրջներ">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Պահանջել կամուրջ">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Միացում BridgeDB-ին։ Խնդրում ենք սպասել։">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Լուծեք «CAPTCHA»-ն կամուրջ պահանջելու համար:">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Լուծումը ճիշտ չէ: Խնդրում ենք փորձել կրկին:">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Տրամադրել կամուրջ">
<!ENTITY torPreferences.provideBridgeHeader "Մուտքագրել թույլատված աղբյուրից կամուջի տեղեկությունը">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Միացման կարգավորումներ">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Կարգավորել թե ինչպես Tor դիտարկիչը միանա համացանցին">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor-ի մատյաններ">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Չի միացվել">
<!ENTITY torConnect.connectingConcise "Միացում…">
<!ENTITY torConnect.tryingAgain "Փորձվում է կրկին:">
<!ENTITY torConnect.noInternet "Tor դիտարկիչը չկարողացավ հասնել համացանցին">
<!ENTITY torConnect.couldNotConnect "Tor դիտարկիչը չկապվեց Tor-ին">
<!ENTITY torConnect.assistDescriptionConfigure "կարգավորեք Ձեր միացումը"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Եթե Tor-ն արգելափակված է Ձեր գտնվելու վայրում, կամուրջ փորձելը կարող է օգնել։ Միացման օգնականը կարող է ընտրել դա Ձեզ համար գտնվելու վայրի հիման վրա, կամ կարեղ եք փոխարենը #1 ձեռքով։"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Փորձվում է կամուրջ...">
<!ENTITY torConnect.tryingBridgeAgain "Փորձվում է ևս մեկ անգամ...">
<!ENTITY torConnect.errorLocation "Tor Browser-ը չկարողացավ տեղորոշել Ձեզ">
<!ENTITY torConnect.errorLocationDescription "Tor Browser-ը ցանկանում է իմանալ Ձեր գտնվելու վայրը ճիշտ կամուրջ ընտրելու համար: Եթե չեք ցանկանում կիսվել Ձեր տեղագրությամբ, #1 դրա փոխարեն ձեռքով:"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Գնտվելու վայրի այս կարգավորումները ճիշտ ե՞ն:">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Միացման օգնական">
<!ENTITY torConnect.breadcrumbLocation "Գտնվելու վայրի կարգավորումներ">
<!ENTITY torConnect.breadcrumbTryBridge "Փորձել կամուրջը">
<!ENTITY torConnect.automatic "Ինքնաբերաբար">
<!ENTITY torConnect.selectCountryRegion "Ընտրեք երկիրը և շրջանը">
<!ENTITY torConnect.frequentLocations "Հաճախակի ընտրված գտնվելու վայրեր">
<!ENTITY torConnect.otherLocations "Այլ գտնվելու վայրեր">
<!ENTITY torConnect.restartTorBrowser "Վերսկսել Tor դիտարկիչը">
<!ENTITY torConnect.configureConnection "Կարգավորել միացումը...">
<!ENTITY torConnect.viewLog "Տեսնել մատյանները...">
<!ENTITY torConnect.tryAgain "Փորձել կրկին">
<!ENTITY torConnect.offline "Համացանցը հասանելի չէ">
<!ENTITY torConnect.connectMessage "Tor-ի կարգավորումներում փոփոխությունները ուժի մեջ կմտնեն միանալուց հետո">
<!ENTITY torConnect.tryAgainMessage "Tor դիտարկիչին չհաջողվեց կազմավորել միացում Tor ցանցին">
<!ENTITY torConnect.yourLocation "Ձեր գտնվելու վայրը">
<!ENTITY torConnect.tryBridge "Փորձել կամուրջ">
<!ENTITY torConnect.autoBootstrappingFailed "Ինքնաբերաբար կարգավորումը ձախողվեց">
<!ENTITY torConnect.autoBootstrappingFailed "Ինքնաբերաբար կարգավորումը ձախողվեց">
<!ENTITY torConnect.cannotDetermineCountry "Անկարող է որոշել օգտագործողի երկիրը">
<!ENTITY torConnect.noSettingsForCountry "Ձեր գտնվելու վայրի համար կարգավորումներ չկան">
