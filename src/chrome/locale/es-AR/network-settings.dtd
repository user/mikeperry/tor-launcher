<!ENTITY torsettings.dialog.title "Configuraciones de la red Tor">
<!ENTITY torsettings.wizard.title.default "Conectar a Tor">
<!ENTITY torsettings.wizard.title.configure "Configuraciones de la red Tor">
<!ENTITY torsettings.wizard.title.connecting "Estableciendo una conexión">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Idioma del Navegador Tor">
<!ENTITY torlauncher.localePicker.prompt "Por favor seleccioná un idioma ">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Cliqueá &quot;Conectar&quot; para conectar a Tor.">
<!ENTITY torSettings.configurePrompt "Cliqueá &quot;Configurar&quot; para modificar los ajustes de red si estás en un país que censura Tor (tales como Egipto, China, Turquía) o si te estás conectando desde una red privada que requiere un proxy.">
<!ENTITY torSettings.configure "Configurar">
<!ENTITY torSettings.connect "Conectar">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Esperando que Tor arranque...">
<!ENTITY torsettings.restartTor "Reiniciar Tor">
<!ENTITY torsettings.reconfigTor "Reconfigurar">

<!ENTITY torsettings.discardSettings.prompt "Has configurado puentes Tor o has entrado ajustes para proxy local.&#160; Para hacer una conexión directa a la red de Tor, éstos ajustes deben ser removidos.">
<!ENTITY torsettings.discardSettings.proceed "Remover ajustes y conectar">

<!ENTITY torsettings.optional "Opcional">

<!ENTITY torsettings.useProxy.checkbox "Uso un proxy para conectar a Internet">
<!ENTITY torsettings.useProxy.type "Tipo de proxy">
<!ENTITY torsettings.useProxy.type.placeholder "seleccioná un tipo de proxy">
<!ENTITY torsettings.useProxy.address "Dirección">
<!ENTITY torsettings.useProxy.address.placeholder "Dirección IP o nombre de equipo">
<!ENTITY torsettings.useProxy.port "Puerto">
<!ENTITY torsettings.useProxy.username "Nombre de usuario">
<!ENTITY torsettings.useProxy.password "Contraseña">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Esta computadora pasa a través de un cortafuegos que sólo permite conexiones a ciertos puertos">
<!ENTITY torsettings.firewall.allowedPorts "Puertos permitidos">
<!ENTITY torsettings.useBridges.checkbox "Tor está censurado en mi país">
<!ENTITY torsettings.useBridges.default "Seleccioná un puente incorporado">
<!ENTITY torsettings.useBridges.default.placeholder "seleccioná un puente">
<!ENTITY torsettings.useBridges.bridgeDB "Solicitá un puente desde torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Entrá los caracteres de la imagen">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Obtené un nuevo desafío">
<!ENTITY torsettings.useBridges.captchaSubmit "Enviar">
<!ENTITY torsettings.useBridges.custom "Proveer un puente que conozco">
<!ENTITY torsettings.useBridges.label "Entrar información de puente desde una fuente confiable.">
<!ENTITY torsettings.useBridges.placeholder "tipo dirección:puerto (uno por línea)">

<!ENTITY torsettings.copyLog "Copiar la bitácora Tor al Portapapeles">

<!ENTITY torsettings.proxyHelpTitle "Ayuda para Proxy">
<!ENTITY torsettings.proxyHelp1 "Un proxy local puede ser necesario cuando te estés conectando a través de una red de una compañía, escuela o universidad.&#160;Si no estás seguro si un proxy es necesario, mirá los ajustes de Internet en otro navegador o revisá tus ajustes de red del sistema.">

<!ENTITY torsettings.bridgeHelpTitle "Ayuda para Repetidor Puente">
<!ENTITY torsettings.bridgeHelp1 "Los puentes son repetidores no listados que hacen más dificultoso bloquear conexiones a la red Tor.&#160; Cada uno de los tipos de puente usa un método diferente para evitar censura.&#160; Los obfs hacen que tu tráfico parezca ruido aleatorio, y los meek hacen que tu tráfico parezca que se está conectando a otro servicio en vez de Tor.">
<!ENTITY torsettings.bridgeHelp2 "Por la manera en que ciertos países intentan bloquear a Tor, ciertos puentes funcionan en ciertos países pero no otros.&#160; Si no estás seguro acerca de cuáles puentes funcionan en tu país, visitá torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Por favor, esperá mientras establecemos una conexión a la red de Tor.&#160;  Puede tardar varios minutos.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Conexión">
<!ENTITY torPreferences.torSettings "Configuraciones de Tor">
<!ENTITY torPreferences.torSettingsDescription "El navegador Tor enruta tu tráfico a través de la red Tor, mantenida por miles de voluntarios en todo el mundo." >
<!ENTITY torPreferences.learnMore "Aprendé más">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Fuera de línea">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Conectado">
<!ENTITY torPreferences.statusTorNotConnected "No Conectado">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Aprendé más">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Arranque rápido">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Siempre conectar automáticamente">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Puentes">
<!ENTITY torPreferences.bridgesDescription "Los puentes te permiten acceder a la red Tor desde lugares donde Tor está bloqueado. Dependiendo de dónde te encuentres, algunos puentes funcionarán mejor que otros.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automática">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Remover">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "¡Copiado!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Solicitá un puente...">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avanzado">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Ver bitácora de sucesos">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Cancelar">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Solicitá un puente">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contactando BridgeDB. Por favor esperá.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Solucioná el CAPTCHA para solicitar un puente.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "La solución no es correcta. Por favor intentá de nuevo.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Entrar información del puente desde una fuente confiable">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Bitácora de eventos de Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "No Conectado">
<!ENTITY torConnect.connectingConcise "Conectando...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automática">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configurar la conexión...">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Try Again">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Los cambios en la configuración de Tor no van a tener efecto hasta que te conectes">
<!ENTITY torConnect.tryAgainMessage "El Navegador Tor ha fallado al establecer una conexión a la red Tor">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Probar un puente">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
