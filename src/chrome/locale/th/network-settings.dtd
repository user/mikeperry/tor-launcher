<!ENTITY torsettings.dialog.title "ตั้งค่าเครือข่าย Tor">
<!ENTITY torsettings.wizard.title.default "เชื่อมต่อกับ Tor">
<!ENTITY torsettings.wizard.title.configure "ตั้งค่าเครือข่าย Tor">
<!ENTITY torsettings.wizard.title.connecting "กำลังสร้างการเชื่อมต่อ">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "ภาษาของ Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "โปรดเลือกภาษา">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "คลิกปุ่ม &quot;เชื่อมต่อ&quot; เพื่อเชื่อมต่อกับ Tor">
<!ENTITY torSettings.configurePrompt "คลิกปุ่ม &quot;กำหนดค่า&quot; เพื่อปรับการตั้งค่าเครือข่ายถ้าคุณอยู่ในประเทศที่เซ็นเซอร์ Tor (เช่น อียิปต์ จีน ตุรกี) หรือถ้าคุณกำลังเชื่อมต่อจากเครือข่ายส่วนตัวที่จำเป็นต้องใช้พร็อกซี">
<!ENTITY torSettings.configure "กำหนดค่า">
<!ENTITY torSettings.connect "เชื่อมต่อ">

<!-- Other: -->

<!ENTITY torsettings.startingTor "กรุณารอให้ Tor เริ่มทำงาน...">
<!ENTITY torsettings.restartTor "เริ่มการทำงานของ Tor ใหม่">
<!ENTITY torsettings.reconfigTor "กำหนดค่าใหม่">

<!ENTITY torsettings.discardSettings.prompt "คุณได้กำหนดค่าสะพานของ Tor หรือคุณได้ใส่การตั้งค่าพร็อกซีท้องถิ่นไว้แล้ว &amp;#160 คุณจำเป็นต้องลบการตั้งค่าเหล่านี้ออกถ้าต้องการสร้างการเชื่อมต่อโดยตรงกับเครือข่ายของ Tor">
<!ENTITY torsettings.discardSettings.proceed "ลบการตั้งค่าและการเชื่อมต่อ">

<!ENTITY torsettings.optional "ไม่จำเป็น">

<!ENTITY torsettings.useProxy.checkbox "ฉันใช้พร็อกซีเพื่อเชื่อมต่อกับอินเทอร์เน็ต">
<!ENTITY torsettings.useProxy.type "ชนิดพร็อกซี">
<!ENTITY torsettings.useProxy.type.placeholder "เลือกชนิดพร็อกซี">
<!ENTITY torsettings.useProxy.address "ที่อยู่">
<!ENTITY torsettings.useProxy.address.placeholder "ที่อยู่ IP หรือชื่อโฮสต์">
<!ENTITY torsettings.useProxy.port "พอร์ต">
<!ENTITY torsettings.useProxy.username "ชื่อผู้ใช้">
<!ENTITY torsettings.useProxy.password "รหัสผ่าน">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP หรือ HTTPS">
<!ENTITY torsettings.firewall.checkbox "คอมพิวเตอร์เครื่องนี้ใช้งานผ่านไฟร์วอลล์ที่อนุญาตให้เชื่อมต่อกับเฉพาะบางพอร์ตเท่านั้น">
<!ENTITY torsettings.firewall.allowedPorts "พอร์ตที่ได้รับอนุญาต">
<!ENTITY torsettings.useBridges.checkbox "Tor ถูกเซ็นเซอร์ในประเทศของฉัน">
<!ENTITY torsettings.useBridges.default "เลือกสะพานที่มีมาให้ในตัว">
<!ENTITY torsettings.useBridges.default.placeholder "เลือกสะพาน">
<!ENTITY torsettings.useBridges.bridgeDB "เลือกสะพานจาก torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "กรอกตัวอักษรที่อยู่ในภาพ">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "รับการท้าทายใหม่">
<!ENTITY torsettings.useBridges.captchaSubmit "ส่ง">
<!ENTITY torsettings.useBridges.custom "จัดหาสะพานที่ฉันรู้จัก">
<!ENTITY torsettings.useBridges.label "กรอกข้อมูลสะพานจากแหล่งที่น่าเชื่อถือ">
<!ENTITY torsettings.useBridges.placeholder "พิมพ์ที่อยู่: พอร์ต (บรรทัดละพอร์ต)">

<!ENTITY torsettings.copyLog "คัดลอกล็อก Tor ไปที่คลิปบอร์ด ">

<!ENTITY torsettings.proxyHelpTitle "ความช่วยเหลือเกี่ยวกับพร็อกซี">
<!ENTITY torsettings.proxyHelp1 "พร็อกซีท้องถิ่นอาจจะมีความจำเป็นหากเชื่อมต่อผ่านเครือข่ายบริษัท โรงเรียน หรือมหาวิทยาลัย &#160; ถ้าคุณไม่แน่ใจว่าจำเป็นจะต้องใช้พร็อกซีหรือไม่ ดูได้ที่การตั้งค่าอินเทอร์เน็ตในเบราวเซอร์อื่น หรือตรวจสอบการตั้งค่าเครือข่ายของระบบของคุณ">

<!ENTITY torsettings.bridgeHelpTitle "การช่วยเหลือเกี่ยวกับรีเลย์สะพาน">
<!ENTITY torsettings.bridgeHelp1 "สะพานเป็นรีเลย์ที่ไม่อยู่ในรายการ ซึ่งทำให้การบล็อกการเชื่อมต่อกับเครือข่าย Tor ทำได้ยากขึ้น &#160; สะพานแต่ละชนิดใช้วิธีการที่แตกต่างกันเพื่อหลบเลี่ยงการเซ็นเซอร์ &#160; สะพานแบบ obfs ทำให้การรับส่งข้อมูลของคุณเป็นเหมือนเสียงรบกวนที่ไม่มีความหมาย ส่วนสะพานแบบ meek จะทำให้การรับส่งข้อมูลของคุณเหมือนกำลังเชื่อมต่อกับบริการนั้นๆ แทนที่จะเป็น Tor">
<!ENTITY torsettings.bridgeHelp2 "เนื่องจากบางประเทศบล็อก Tor สะพานบางอันจึงใช้งานได้เฉพาะในบางประเทศเท่านั้น &#160; ถ้าคุณไม่แน่ใจว่าสะพานอันไหนใช้งานได้ในประเทศของคุณบ้าง กรุณาดูที่ torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "กรุณรอระหว่างที่เรากำลังสร้างการเชื่อมต่อไปยังเครือข่าย Tor &#160; ซึ่งอาจจะใช้เวลาหลายนาที">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "การเชื่อมต่อ">
<!ENTITY torPreferences.torSettings "การตั้งค่า Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser จะกำหนดเส้นทางการรับส่งข้อมูลของคุณเข้ากับเครือข่าย Tor ที่ทำงานโดยอาสาสมัครกว่าพันคนทั่วโลก" >
<!ENTITY torPreferences.learnMore "เรียนรู้เพิ่มเติม">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "ทดสอบ">
<!ENTITY torPreferences.statusInternetOnline "ออนไลน์">
<!ENTITY torPreferences.statusInternetOffline "ออฟไลน์">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "เชื่อมต่อสำเร็จ">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "เรียนรู้เพิ่มเติม">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "เริ่มต้นโดยรวดเร็ว">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "เชื่อมต่อโดยอัตโนมัติตลอด">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "บริดจ์">
<!ENTITY torPreferences.bridgesDescription "Bridges help you access the Tor Network in places where Tor is blocked. Depending on where you are, one bridge may work better than another.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatic">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "ลบ">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Copied!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "ขอสะพาน…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "ขั้นสูง">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "ดูไฟล์บันทึก">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "ยกเลิก">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "เกล็ดหิมะ">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "ขอบริดจ์">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contacting BridgeDB. Please Wait.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "แก้ปัญหา CAPTCHA เพื่อขอสะพาน">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "วิธีแก้ไขไม่ถูกต้อง กรุณาลองอีกครั้ง">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor ไฟล์บันทึก">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "กำลังเชื่อมต่อ...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatic">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "ลองอีกครั้ง">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
